package br.sp.gov.fatec;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import br.gov.sp.fatec.model.Filme;
import br.gov.sp.fatec.repository.FilmeRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext.xml" })
@Rollback
@Transactional
public class FimeRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests{


	@Autowired
	private FilmeRepository filmeRepo;
	
	
	public void setUsuarioRepo(FilmeRepository usuarioRepo) {
		this.filmeRepo = usuarioRepo;
	}

	@Test
	public void testeInsercaoOk() {
		Filme filme1 = new Filme();
		filme1.setNome("Clube da Luta");
		filme1.setDiretor("David Fincher");
		filme1.setGenero("Drama");
		filme1.setAtores("Brad Pitt, Edward Norton, Meat Loaf");
		filme1.setRoteirista("Chuck Palahniuk, Jim Uhls");
		
		filmeRepo.save(filme1);
		assertTrue(filme1.getId() != null);
	}
}
